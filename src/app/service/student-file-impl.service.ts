import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable } from '../../../node_modules/rxjs';
import Student from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class StudentFileImplService extends StudentService{

  constructor() {
    super();
  }
   getStudents(): Observable<Student[]>{return null};
     getStudent(id: number): Observable<Student>{return null};
}
