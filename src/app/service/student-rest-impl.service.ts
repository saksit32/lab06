import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable } from '../../../node_modules/rxjs';
import Student from '../entity/student';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentRestImplService extends StudentService {

  constructor(private http: HttpClient) {
    super();
  }
  getStudents(): Observable<Student[]> { return this.http.get<Student[]>(environment.studentApi) };
  getStudent(id: number): Observable<Student> { return this.http.get<Student>(environment.studentApi + id) };
}