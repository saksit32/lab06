import { TestBed, inject } from '@angular/core/testing';

import { StudentRestImplService } from './student-rest-impl.service';

describe('StudentRestImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentRestImplService]
    });
  });

  it('should be created', inject([StudentRestImplService], (service: StudentRestImplService) => {
    expect(service).toBeTruthy();
  }));
});
